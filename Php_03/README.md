# Php_02 | Funktioner

# Læringsmål
## Viden
### - Viden om funktioners anvendelsesområde
### - Viden om scope, herunder variable scope

## Færdigheder
### - kan anvende gængse indbyggede funktioner
### - kan deklarere egne funktioner
### - kan anvende parametre
### - kan anvende funktioner der returnerer værdier
### - kan anvende anonyme funktioener

# Forberedelse (Skal forberedes inden fremmøde)

## Læsning (Programming PHP, third edition. )
### Kapitel 3

## Video (Se inden fremmøde)
### 

## Opgaver (Bedes løst inden fremmøde)
### 

# Dagens agenda
## 08:10 - 08:45 | Gennemgang af foregående elementer
## 08:45 - 09:30 | Array
## 10:00 - 11:00 | Opgaver
## 11:00 - 11:30 | Gennemgang af opgaver
## 12:00 - 12:30 | Control Flow
## 12:30 - 15:00 | Opgaver i kontrol flow