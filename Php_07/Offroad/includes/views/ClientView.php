<aside class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <a href="#">
                    <img src="http://www.energibyenskive.dk/frontend/images/img_skive-kommune.gif" class="img-responsive img-centered" alt="">
                </a>
            </div>
            <div class="col-md-6 col-sm-6">
                <a href="#">
                    <img src="http://infodatabaser.eadania.dk/Grafik/logo_dania_web_u_skygge_200px.gif" class="img-responsive img-centered" alt="">
                </a>
            </div>
        </div>
    </div>
</aside>