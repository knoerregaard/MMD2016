<?php
    /* Datatyper
     * string eller tekststrengværdier
    */

    $a = "Dette er en tekststreng<br>";
    echo $a;

    $a = 'Dette er en tekststreng<br>';
    echo $a;

    $name = "Klaus";
    echo "Hej, $name";
    echo "<br>";
    echo 'Hej, $name';

    $a = "<p>Dette er en tekststreng der indeholder afsnitselementer</p>";
    echo $a;
    
    date_default_timezone_set("Europe/Copenhagen");
    $a = "<div>Dato i dag er: " . date("D m y") . " og kl. er: " . date("h:i:s");
    echo $a;

    //Teste om to strenge er ens
    echo "<br>";
    echo "Er de to variabler ens?<br>";
    $a = "abc";
    $b = "abc1";
    

    if($a == $b)
    {
        echo 'ja, $a og $b er ens <br>';
    }else {
        echo "de er forskellige!"
    }
?>