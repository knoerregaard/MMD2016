<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstName = "Klaus";

    //tekststreng (string)
    $lastName = "Nørregaard";

    //nummerisk heltal (integer)
    $age = 36;

    //boolean (sandt/falsk)
    $inRelationship = true;

    //tekststreng
    $work = "Underviser";

    //tekststreng
    $workPlace = "Erhvervsakademi Dania";

    //array (en række)
    $hobbies = ["Computer","Kodning","Vandsport"];

?>