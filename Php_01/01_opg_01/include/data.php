<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     //Simpel streng
     $email = "kl@eadania.dk";

     //Simpel int.
     $phone = 41770410;
     
     //Et simpelt array, der kan tilgås ved hjælp af index.
     $preferences = array('Programmering','Organisation og udvikling','Undervisning og læring');

     //Et associativt array, der består af en key og en value.
     $jobs = array('Fiat Herning' => 'Hjemmeside','Scholl sko' => 'Hjemmeside og webshop','Grameta' => 'Hjemmeside og webshop','N Graversen' => 'Hjemmeside og digitale strategier');

     //Et kompliceret array, der består af simple elementer og simple array. 
     $competencies = array('Undervisning',array('PHP','C#','HTML','CSS'), array('AngularJS','Bootstrap','Foundation'));

?>