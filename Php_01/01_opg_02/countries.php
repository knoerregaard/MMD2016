<?php
    /*
     * Array
     * Opsæt et accosiativt array der indeholder byer og deres hovedstader.
     * Der skal minimum være 20 lande og deres respektive hovedstader i arrayet.
     * "Italy"=>"Rome"
     * Når Arrayet er opsat, skal du udskrive arrayet tre gange.
     * 1. gang skal arrayet udskrives i den rækkefølge, som angivet i arrayet.
     * 2. gang skal arrayet udskrives, således, at landenavne er sorteret efter forbogstav. Lande med A i forbogstav skal komme først.
     * 3. gang skal arrayet udskrives, således, at bynavnen er sorteret efter forbogstav.
     * Få hjælp her: http://php.net/manual/en/array.sorting.php
     * I mappen ligger landeOgByer.png hvor du kan se udskriften for arrayet der ikke er sorteret.
     */

    $countries = array( "Italy"=>"Rome", "Denmark"=>"Copenhagen","Frankrig"=>"Paris", "Holland"=>"AmsterDam", "Indien"=>"New Delhi", "Kina"=>"Beijing");
    
    var_dump($countries);
    //asort($countries);
    //ksort($countries);
    foreach ($countries as $key => $value) {
        echo "Land: " . $key . " By: " . $value . "<br>";
    }
    

    /*
     * Ekstra opgave
     * Tilføj flere elementer til arrayet. Brug array_push()
     */

    $countries += array("denmark"=>"copenhagen");
?>