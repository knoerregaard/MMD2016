# Php_01 | Introduktion til php programmering,  klient-server, samt basal programmering med php
Vi benytter bogen Programming Php, third edition
På første dag beskæftiger vi os med en introduktion til PHP.

# Læringsmål
## Viden
### - Viden om programmeringssprogets anvendelsesmuligheder
### - Viden om php serverens (Apache) rolle i et klient-server forhold
### - Viden om foreskellene på klientside kode og serverside kode
### - Viden om foreskellene på et scriptsprog og et klassisk programmeringssprog
### - Viden om phps sproglige struktur (leksikal struktur, basale sprogregler)

# Forberedelse (Skal forberedes inden fremmøde)

## Læsning (Programming PHP, third edition. )
### Chapter 1. Introduction to PHP (Side )
### What Does PHP Do? (Side )
### A Brief History of PHP (Side )
### A Walk Through PHP (Side )


## Video (Se inden fremmøde)
### Opret en konto på Gitlab
### Hent og installer GIT
### Hent og installer FileZilla
### Opret et websted i FileZilla der peger på Php serveren

## Opgaver (Bedes løst inden fremmøde)
### Opret en konto på Gitlab
### Hent og installer GIT
### Hent og installer Filezilla på
### Opret et Websted i FileZilla der peger på Php serveren
### Individuelt skal i redegøre for jeres udfordringer i relation til programmering ved fx javascript, således bliver dette udgangspunkt for debat.

# Dagens agenda
## 08:10 - 08:45 | Gennemgang af foregående elementer
## 08:45 - 09:30 | Array
## 10:00 - 11:00 | Opgaver
## 11:00 - 11:30 | Gennemgang af opgaver
## 12:00 - 12:30 | Control Flow
## 12:30 - 15:00 | Opgaver i kontrol flow