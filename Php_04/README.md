# Php_03 | Påbegyndelse af Offroad

# Læringsmål
## Viden
### - Objekter i den virkelige verden, og objekter i den digitale verden

## Færdigheder
### - Nedskrive user stories
### - Nedbryde user stories i opgaver
### - 

# Forberedelse (Skal forberedes inden fremmøde)

## Læsning (Programming PHP, third edition. )
### Kapitel 3

## Video (Se inden fremmøde)
### 

## Opgaver (Bedes løst inden fremmøde)
### 

# Dagens agenda
## 1. Gennemgang af Offroad og behov. Udtrykt ved user stories.
## 1.1. Hvad kender de studerede til OffRoad 
## 1.2. Hvad skal vores applikation skulle kunne?
## 1.4. User stories
## 2. Påbegyndelse af OffRoadProjekt

#User stories til OffRoad
## Som bruger, vil jeg gerne kunne se aktuelle brugerhistorier, således, at jeg kan få nogle intryk fra allerede gennemførte arrangementer
## Som bruger, vil jeg gerne kunne tilføje en ny brugerhistorie, således, at jeg kan dele indtryk fra allerede gennemførte arrangementer
## Som bruger, vil jeg gerne kunne tilføje et billede til en brugerhistorie, således, at når brugerhistorien læses af andre er billedet også vist
## Som bruger, vil jeg gerne kunne tilføje en video til en brugerhistorie, således, at når brugerhistorien læses af andre er videoen også vist
## Som bruger, vil jeg gerne kunne tilføje en kommentar til en eksisternede brugerhistorie, således, at historiens fortælling udviddes med andres perspektiver.
## Som bruger, vil jeg gerne kunne slette en brugerhistorie, således, at når historien ikke længere er relevant
## Som besøgende, vil jeg gerne kunne logge ind i systemet, således, at jeg kan ligge mine egne brugerhistorier op.
## Som bruger, vil jeg gerne kunne slette min konto fra applikationen, således, at jeg ikke længere er aktiv på applikationen.
## Som bruger, vil jeg gerne kunne søge i allerede oprettede brugerhistorier
