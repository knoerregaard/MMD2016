#PHP-sprogets basale elementer
Kapitel 2

## Viden
### - Viden om datatyper
### - Viden om basale programmeringselementer i php, herunder variabler, echo
## Færdigheder
### - Kan anvende Echo
### - Kan anvende en FTP klient til uploade af HTML og Php kode
### - Kan indlejre php script i html
### - Kan oprette validerede dokumenter hvor i indlejret php script kode finder sted
### - Kan deklarere variabler i forskellige sammenhængen
### - Kan anvende operatorer
### - Kan oprettet et repo på Gitlab, samt anvende GIT til dokumenthåndtering

## Pensum
### Chapter 2. Language Basics (Side )
### Lexical Structure (Side )
### Data Types (Side )
### Variables (Side )
### Expressions and Operators (Side ) (Læs ikke )
### Flow-Control Statements (Side )
### Including Code (Side )
### Embedding PHP in Web Pages (Side )