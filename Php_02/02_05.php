<?php

    /*
    * Flow kontrol.
    * IF 
    * Elseif
    * Else
    * 
    * If(expression) statement
    * If sætninger tester om udtrykket returnerer sandt eller falsk. 
    * Hvis udtrykket returnere sandt, så udføres erklæringen (statement)
    */

    //En bruger er logget ind.
    $userIsLoggedIn = true;
    //Så tjekker vi om om udtrykket returnerer sandt. Hvis ikke, så eksekveres erklæringen under else.
    if ($userIsLoggedIn){
        echo "Welcome!";
    }else {
        echo "Access Forbidden!";
    }
    echo "<br>";

    //If sætninger der returnerer sandt, hvis udtrykket er korrekt.
    if(date('D') == 'Mon')
    {
        echo "I dag er det Manday";
    }elseif (date('D') == 'Tue')
    {
        echo "I dag er det Tirsdag";
    }elseif (date('D') == 'Wed')
    {
        echo "I dag er det Onsdag";
    }elseif (date('D') == 'Thu')
    {
        echo "I dag er det Torsdag";
    }elseif (date('D') == 'Fri')
    {
        echo "I dag er det Fredag";
    }elseif (date('D') == 'Sat')
    {
        echo "I dag er det Lørdag";
    }elseif (date('D') == 'Sun')
    {
        echo "I dag er det Søndag";
    }
    else {
        echo "Det er åbenbart ikke nogen af dagene.";
    }

?>
