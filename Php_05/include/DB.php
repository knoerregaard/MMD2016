<?php

    /* Database klasse der indeholder metoder */
    
    //config.php indeholder db specifikke oplysninger
    require_once("config.php");
    
    class MySqlDatabase {
        private $connection;
        
        function __construct(){
           $this->open_connection();
        }

        public function prepare(){
            
        }

        //Åben forbindelse
        public function open_connection(){
            $this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
            if(mysqli_connect_errno()){
                die("Fejl: Der kunne ikke oprettes forbindelse til databasen. " . 
                    mysqli_connect_errno() . " (" . mysqli_connect_errno() . ") "
                );
            }
        }
        //Luk forbindelse
        public function close_connection(){
            if(isset($this->connection)){
                mysqli_close($this->connection);
                unset($this->connection);
            }
        }

        //Databaseforspørgsel - insert
        public function insert($sql){
            mysqli_query($this->connection, $sql);
        }

        //Databaseforspørgsel - select
        public function query($sql){
            $result = mysqli_query($this->connection, $sql);
            $this->confirm_query($result);
            return $result;
        }
        
        //Metode der tjekker om databasen returnere med et resultat eller ej
        //Private da den kun benyttes inde fra
        private function confirm_query(){
            if(result){
                die("Fejl: Databaseforespørgsel mislykkedes.");
            }
        }
        
        //Metode der sikre vores sql string
        public function mysql_prep($string){
            $escaped_string = mysqli_real_escape_string($this->connection, $string);
            return $escaped_string;
        }
        
        /* Database neutral methods */
        public function fetch_array($result_set){
            return mysqli_fetch_array($result_set);
        }
        
        public function escape_value($string){
            return mysqli_escape_string($string);
        }
        
        public function num_rows($result_set){
            return mysqli_num_rows($result_set);
        }
        
        public function insert_id(){
            return mysqli_insert_id($this->connection);
        }
        
        public function affected_rows(){
            return mysqli_affected_rows($this->connection);
        }
        
    }
    
    $database = new MySqlDatabase();
    
?>