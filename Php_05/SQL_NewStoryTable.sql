CREATE TABLE `offroad2017`.`story` (
  `StoryID` INT NOT NULL AUTO_INCREMENT,
  `StoryName` VARCHAR(45) NULL,
  `StoryDescription` VARCHAR(250) NULL,
  `StoryImg` VARCHAR(45) NULL,
  `StoryLat` FLOAT NULL,
  `StoryLong` FLOAT NULL,
  `StoryDate` TIMESTAMP NULL,
  PRIMARY KEY (`StoryID`)
);
