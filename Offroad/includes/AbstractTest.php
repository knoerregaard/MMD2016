<?php 

    abstract class abstractTrick
    {
        public $store;
        //abstract public functiontrick($whatever);
    }

    class OneTrick extends abstractTrick
    {
        public function trick($whatever)
        {
            $this->store = "an abstract class";
            return $whatever . $this->store;
        }
    }
    
    $worker = new OneTrick();
    echo $worker->trick("From an abstract origin.")


?>