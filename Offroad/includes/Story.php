
<?php
        class Story
        {

            public $stories = array(
                array("StoryId"=>"1","StoryDate"=>"13. marts 2011", "StoryName"=>"Opera at the beach","StoryDescription"=>"Dejlig begivnehed jeg deltog", "StoryImg"=>"../ressources/img/gåtur.jpg"),
                array("StoryId"=>"2","StoryDate"=>"14. marts 2011", "StoryName"=>"Metal at the beach","StoryDescription"=>"Dejlig begivnehed jeg deltog", "StoryImg"=>"../ressources/img/dyk.jpg"),
                array("StoryId"=>"3","StoryDate"=>"15. marts 2011", "StoryName"=>"Hip Hop at the beach","StoryDescription"=>"Dejlig begivnehed jeg deltog", "StoryImg"=>"../ressources/img/dyk.jpg"),
                array("StoryId"=>"4","StoryDate"=>"16. marts 2011", "StoryName"=>"Tekno at the beach","StoryDescription"=>"Dejlig begivnehed jeg deltog", "StoryImg"=>"../ressources/img/dyk.jpg"),
                array("StoryId"=>"5","StoryDate"=>"17. marts 2011", "StoryName"=>"Country at the beach","StoryDescription"=>"Dejlig begivnehed jeg deltog", "StoryImg"=>"../ressources/img/dyk.jpg")
            );

            function returnAllStories()
            {
                $storyCount = count($this->stories);
                //Funktion der returnerer alle historier fra arrayet.
                foreach ($this->stories as $story) {
                    $storyCount--;
                    if ($storyCount % 2 == 0)
                    { 
                        echo "<li class='timeline-inverted'>";
                    }else {
                        echo "<li>";
                    };
                    echo "<div class='timeline-image'>";
                    echo "<img class='img-circle img-responsive' src='" . $story["StoryImg"] . "' alt=''>";
                    echo "</div>";
                    echo "<div class='timeline-panel'>";
                    echo "<div class='timeline-heading'>";
                    echo "<h4>" . $story["StoryDate"] . "</h4>";
                    echo "<h4 class='subheading'>" . $story["StoryName"] . "</h4>";
                    echo "</div>";
                    echo "<div class='timeline-body'>";
                    echo "<p class='text-muted'>" . $story["StoryDescription"] . "</p>";
                    echo "</div>";
                    echo "</div>";
                    echo "</li>";
                }
            }
            //Denne metode forespørger på alle historierne i databasen
            function getAlleStories(){

            }
        }
?>