<?php 
    /* Baseclass User */
    
    require_once('DB.php');
    
    class User{
        public $id;
        public $firstName;
        public $lastName;
        public $email;
        public $password;
        
        //Konstruktøren opretter et validt objekt på instantieringstidspunktet.
        function __construct($id, $firstName, $lastName, $email, $password){
            $this->$id = $id;
            $this->$firstName = $firstName;
            $this->$lastName = $lastName;
            $this->$email = $email;
            $this->$password = $password;
        }
        
        //Metode der opretter objekter. Factory pattern
        public static function create(){
            
        }
        
        //Find alle brugere i db. Når de kommer tilbage i et array konverteres de til et objekt array før de returneres
        public static function find_all(){
            global $database;
            $result_set = $database->query("SELECT * FROM users");
            return $result_set;
            //Fra et array med rækker til et array med objekter
            $users = array();
            while ($row = $database->fetch_array($result_set)) {
                $user = new User();
                $users[] = $user;
            }
            return $users;
        }
        
        //Find brugeren med et specifikt id
        public static function find_by_id($userid){
            global $database;
            $result_set = $database->query("SELECT * FROM users WHERE UserId={$userid} LIMIT = 1");
            $found = $database->fetch_array($result_set);
            return $found;
        }
        
        //SQL 
        public static function find_by_sql($sql_string){
            //
        }
        
        //Opret ny bruger
        //public function create(){
            
        //}
        
        //Opdater eksisterende bruger
        public function update(){
            
        }
        
        
        public function delete(){
            
        }
        
    }
    
?>