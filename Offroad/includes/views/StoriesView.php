<style>
.timeline-image {
    height:100px;
    width:100px;
    overflow: hidden;
    radius:5px;
}

.img-circle {
    width: 300px!;
    margin: 00px 0 0 0px;

}
</style>
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Historier fra brugere</h2>
                    <h3 class="section-subheading text-muted">Seneste</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                    <?php
                        include("../includes/Story.php");
                        $st = new Story();
                        $st->returnAllStories();
                    ?>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Se flere
                                    <br>indslag
                                    <br>
                                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>

                                </h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
