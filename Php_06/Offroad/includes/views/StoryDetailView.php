<!-- Use the modals below to showcase details about your portfolio projects! -->

<!-- Portfolio Modal 1 -->
<br><br><br><br><br><br>
<div class="container">
    <div class="row">
    
        <div class="col-lg-8">
            <?php
                include_once("../includes/Story.php");
                $st = new Story();
                $st->returnStoryDetailView($_GET["id"]);
            ?>
        </div>
        <div class="col-lg-4">
        <h2>Kommenter</h2>
             <form>
                <div class="form-group">
                    <label for="comment">Kommentar</label>
                    <textarea class="form-control" name="comment" form="usrform"></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Dit navn:</label>
                    <input type="name" class="form-control" id="name">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <hr>
            <p style="font-weight:bold">Anders and | <span>1. januar 2016</span></p>
            <p><i>Den gode oplevelse</i></p>
            <hr>
            <p style="font-weight:bold">Mickey | <span>12. januar 2016</span></p>
            <p><i>Den gode oplevelse blev til en fantastisk oplevelse</i></p>
            <hr>
        </div>
    </div>
</div>
