
<?php
        /*
        * Storyklassen skal indeholde alle metoder der håndtere brugerhistorier.
        * Storyklassen skal fx sørge for at brugergrænsefladen populeres med historier og enkelte historier.
        * Vi blander objektorienteret programmering med funktional programering, da vi ikke anvender os af klasse instantiering 
        * på dette niveau. VI benytter os ligeledes heller ikke af Konstruktører eller properties.
        * Vi benytter klassen til indkapsling og logisk opdeling af metoder.
        */

        //Til stort set alle metoder skal vi bruge database. Derfor inkluderer vi DB.php i denne klasse.
        require_once("DB.php");
        
        //Story er en klasse - En klasse er en klassifikation der indeholder 
        // associerede funktioner/metoder og/eller egenskaber.
        //Indkapsle logikken i applikationen.
        class Story
        {
            public static function request(){
                //$_SERVER har variabler. Fx REQUEST_METHOD.
                //I nedenstående tilfælde ser vi på om variablens værdi 
                //er lig med 'POST'
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $story = new Story();
                    $story->postNewStory();
                }
            }

            /* returnAllStoriesView() */ 
            //Metode der returnere alle historier fra database.
            //Metoden anvendes i StoriesView.php 
            public static function returnAllStoriesView()
            {
                //Der oprettes en instans, som vi skal bruge når vi skal have fat i $conn.
                $db = new DB();

                // Formålet med returnAllStoriesView er at returnere alle historier fra databasen.
                // Vi definerer en variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
                $sql = "SELECT * FROM story";

                // Før benyttede vi $conn da vi skulle forsøge at få adgang til serveren.
                // Nu skal vi rent faktisk have nogle data retur. Derfor benytter vi nu $conn og en metode der hedder query()
                // til at hente data.
                // Resultatet fra forespørgslen gemmes i $result variablen.
                $result = $db->conn->query($sql);
                
                // I brugergrænsefladen er hver anden historier højrecentret. Vi tæller derfor antallet af rækker
                // til senere brug.
                $storyCount = $result->num_rows;
                
                //While løkke der returnerer alle historier fra arrayet.
                // $row = $result->fetch_assoc() returnere hver eneste række som et associativt elementer. Fx "StoryID" : "1"
                while($row = $result->fetch_assoc()) {
                    // De næste seks linjer benyttes til at bestemme om historien skal højre eller venstrecentreres.
                    // % er modulus tegnet. Hvis der fx gemmer sig 2 bag StoryCount er modulus 2 = 0. Men hvis der gemmer sig 3 bag storyCount
                    // vil modulus 2 være forskellig fra nul. Og dermed hopper if ned i else blokken.
                    $storyCount--;
                    if ($storyCount % 2 == 0)
                    {
                        echo "<li class='timeline-inverted'>";
                    }else {
                        echo "<li>";
                    };
                    // Hver eneste key fra $row indsættes i html hvor det giver emning. Således vises værdien når brugeren ser siden.
                    echo "<div class='timeline-image'>";
                    echo "<img class='img-circle img-responsive' src='../resource/img/" . $row["StoryImg"] . "' alt=''>";
                    echo "</div>";
                    echo "<a href='storydetail.php?id=" . $row["StoryID"] . "'>";
                    echo "<div class='timeline-panel'>";
                    echo "<div class='timeline-heading'>";
                    echo "<h4>" . $row["StoryDate"] . "</h4>";
                    echo "<h4 class='subheading'>" . $row["StoryName"] . "</h4>";
                    echo "</div>";
                    echo "<div class='timeline-body'>";
                    echo "<p class='text-muted'>" . $row["StoryDescription"] . "</p>";
                    echo "</div>";
                    echo "</a>";
                    echo "</div>";
                    echo "</li>";
                }
            }
            /* returnStoryDetailView() */ 
            // Denne metode forespørger på én specifik historie.
            // Vi har et View der hedder StoryDetailView.php. Formålet med dette view er, at vise detaljer
            // om en specifik historie
            function returnStoryDetailView($storyid){
                $db = new DB();
                
                // Denne sql sætning hender alle kollonner fra story tabellen, hvor StoryID = storyid.
                $q = "SELECT * FROM story WHERE StoryID = $storyid";
                
                $result = $db->conn->query($q);
                $value = mysqli_fetch_assoc($result); //Omskrive object til assocciativt array
                echo "<h2>" . $value["StoryName"] . "</h2>";
                echo "<figure>";
                echo "<img class='img-responsive img-centered figure-img img-fluid img-rounded' src='../resource/img/" . $value["StoryImg"] . "' alt=''>";
                echo "<figcaption class='figure-caption'>" . $value["StoryImgCaption"] . "</figcaption>";
                echo "</figure>";
                echo "<br>";
                echo "<p>" . $value["StoryDescription"] . "</p>";
            }
            
            function postNewStory()
            {
                //Databasekontekst
                $db = new DB();

                //Variabler (opsætte)
                $storyname = $_POST["StoryName"];
                $storydescription = $_POST["StoryDescription"];
                $storydate = date("Y-m-d H:i:s");
                $storylat = $_POST["StoryLat"];
                $storylong = $_POST["StoryLong"];

                //komponerer sql forespørgsel
                $q = "INSERT INTO story(StoryName, StoryDescription, StoryDate, StoryLat, StoryLong) VALUES('$storyname', '$storydescription', '$storydate', '$storylat', $storylong)";
                
                //Eskeverer sql forspørgsel op i mod databasen
                $db->conn->query($q);
                //Så skulle data være i db.
                //insert_id giver os id på sidste indsatte række i Databasen.
                Header("Location: ../public/storydetail.php?id=" . $db->conn->insert_id);
            }
        }
    // Fordi metode/function request() i Story klassen er sat til
    // static, behøver vi ikke, at oprette en instans/ et objekt at klassen Story
    Story::request();
?>