<?php

    $storyname = $_GET["storyname"];
    $storydescription = $_GET["storydescription"];
    //Lokale variabler
    //Variablerne bruges til forbindelsen DB serveren
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "offroad2017";

    // Opret forbindelse
    $conn = new mysqli($servername, $username, $password, $dbname);
    // tjek om der er forbindelse
    if ($conn->connect_error) {
        die("Forbindelsen fejlede: " . $conn->connect_error);
    }
    
    //En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)

        $sql = "SELECT * FROM story WHERE StoryName = '$storyname'";


    //Resultatet fra forespørgslen gemmes i $result variablen.
    $result = $conn->query($sql);
    
    //Udskriver data (hvis der er noget i $result)
    if ($result->num_rows > 0) {
        // output data fra hver række
        while($row = $result->fetch_assoc()) {
            echo "Navn: " . $row["StoryName"]. " - Beskrivelse: " . $row["StoryDescription"]. " " . $row["StoryDate"] . " " . $row["StoryImg"] . "<br>";
        
        }
    } else {
        echo "Ingen resultater fra databasen";
    }

    //Lukker forbindelsen
    $conn->close();
?> 