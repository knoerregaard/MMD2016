<?php
    /*
    * $_COOKIE[]
    * Indeholder værdier fra
    */
    $value = 'En eller anden tilfældig værdi';

    setcookie("TestCookie1", $value);
    //Hvorfor vil den ikke udskrive værdien første gang man køre dokumentet?
    echo $_COOKIE["TestCookie1"];

    echo "<br>";

    //En cookies levetid kan sættes ved paramter #3
    
    // setcookie("TestCookie2", $value, time()+3600, "/");  /* udløber efter 1 time */
    // echo $_COOKIE["TestCookie2"];

    // echo "<br>";

    // //En cookie kan sætte for hele sitet eller et specifikt biblioktek
    
    // setcookie("TestCookie3", $value, time()+3600, "/");  /* udløber efter 1 time */
    // echo $_COOKIE["TestCookie3"];


?>