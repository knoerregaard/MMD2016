<!-- Use the modals below to showcase details about your portfolio projects! -->

<!-- Portfolio Modal 1 -->
<br><br><br><br><br><br>
<div class="container">
    <div class="row">
    
        <div class="col-lg-8">
            <?php
                include_once("../includes/Story.php");
                $st = new Story();
                $st->returnStoryDetailView($_GET["id"]);
            ?>
            <?php
                include_once("../includes/Comment.php");
                $com = new Comment();
                $com->returnComments($_GET["id"]);
            ?>
            
        </div>
    </div>
</div>
