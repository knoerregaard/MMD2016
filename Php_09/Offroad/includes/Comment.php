<?php
    /* Kommentar klassen */

    //Til stort set alle metoder skal vi bruge database. Derfor inkluderer vi DB.php i denne klasse.
    require_once("DB.php");
    
    //Klassenavn
    class Comment
    {
        //Vi skal benytte en requestmetode, der eventuelt peger på en postmetode
        public static function request(){
            //$_SERVER har variabler. Fx REQUEST_METHOD.
            //I nedenstående tilfælde ser vi på om variablens værdi 
            //er lig med 'POST'
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $comment = new Comment();
                $comment->postNewComment();
            }
        }

        public function postNewComment(){
            echo "asd";
            //Denne metode poster en ny kommentar til databasen
            $db = new DB();

            //Disse posts variabler kommer fra dokumentets header.
            $postName = $_POST["postname"];
            $postText = $_POST["posttext"];
            $postDate = date("Y-m-d H:i:s");
            $storyId  = $_POST["storyId"];

            //Insert streng.
            $q = "INSERT INTO Comments(CommentName, CommentText, CommentDate, StoryId) VALUES('$postName', '$postText', '$postDate', $storyId)";

            //Selve handlingen, at indsætte data i DB.
            $db->conn->query($q);
            //Så skulle data være i db.
            //insert_id giver os id på sidste indsatte række i Databasen.
            Header("Location: ../public/storydetail.php?id=" . $storyId);
        }

        public function returnComments($storyid){
            //Formålet med denne metode, er at returnere data fra DB->Comments 
            //og indsætte disse i HTML

            //Databasekontekst
            $db = new DB();

            //Forespørgsel
            $q = "SELECT * FROM comments WHERE StoryId = $storyid ORDER BY CommentDate DESC LIMIT 2";

            //Resultatsæt der kommer fra databasen.
            $result = $db->conn->query($q);
            //Vi løber arrayet igennem og udskriver nogle html tags. Erstatter
            //de statiske værdier med dynamisk data.
            while($value = $result->fetch_assoc()) {
                echo "<hr>";
                echo "<p style='font-weight:bold'>" . $value["CommentName"] . "| <span>" . $value["CommentDate"] . "</span></p>";
                echo "<p><i>" . $value["CommentText"] . "</i></p>";
            }
        }
    }

    // $story = new Story();
    // $story->removeNewComment();
    Comment::request();
?>